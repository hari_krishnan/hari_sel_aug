package week1.Day2;

import java.util.Scanner;

public class SumOfNosInAnArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scr = new Scanner(System.in);
		int array[] ;
		System.out.println("Enter the length of the array");
		int length = scr.nextInt();
		array = new int[length];
		System.out.println("Enter the array");
		int sum=0;
		for(int i=0;i<length;i++)
		{
			array[i]= scr.nextInt();
			sum=sum+array[i];
		}
		System.out.println(sum);
	}

}
