package week2.Day1;

import java.util.Scanner;

public class PrintPattern {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the nos");
		int a = scan.nextInt();
		int b = scan.nextInt();
		for(int i=a;i<=b;i++)
		{
			if((i%3==0)&&(i%5==0))
			{
				System.out.println("FIZZBUZZ");
				continue;
			}
			if((i%3!=0)&&(i%5!=0))
			{
				System.out.println(i);
				continue;
			}
			else if(i%3==0)
			{
				System.out.println("FIZZ");
				continue;
			}
			else if(i%5==0)
			{
				System.out.println("BUZZ");
				continue;
			}
		}
	}

}
