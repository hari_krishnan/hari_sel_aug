package week3.Day1;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Irctc_registration {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		//driver.findElementByLinkText("REGISTER").click();
		driver.findElementById("userRegistrationForm:userName").sendKeys("TEST_123");
		driver.findElementById("userRegistrationForm:password").sendKeys("TEST_123");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("TEST_123");
		WebElement src = driver.findElementById("userRegistrationForm:securityQ");
		Select dropdown = new Select(src);
		dropdown.selectByVisibleText("----Select your Security Question----");
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("TEST_123");
		WebElement src1 = driver.findElementById("userRegistrationForm:prelan");
		Select dropdown1 = new Select(src1);
		dropdown1.selectByVisibleText("English");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("TEST_123");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("TEST_123");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("TEST_123");
		driver.findElementById("userRegistrationForm:email").sendKeys("TEST_123@gmail.com");
		driver.findElementById("userRegistrationForm:gender:0").click();
		driver.findElementById("userRegistrationForm:maritalStatus:1").click();
		driver.findElementById("userRegistrationForm:resAndOff:0").click();
		WebElement src6 = driver.findElementById("userRegistrationForm:nationalityId");
		Select dropdown6 = new Select(src6);
		dropdown6.selectByVisibleText("India");
		WebElement src7 = driver.findElementById("userRegistrationForm:countries");
		Select dropdown7 = new Select(src7);
		dropdown7.selectByVisibleText("India");		

		WebElement src2 = driver.findElementById("userRegistrationForm:dobDay");
		Select dropdown2 = new Select(src2);
		dropdown2.selectByVisibleText("01");		
		WebElement src3 = driver.findElementById("userRegistrationForm:dobMonth");
		Select dropdown3 = new Select(src3);
		dropdown3.selectByVisibleText("MAY");			
		WebElement src4 = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select dropdown4 = new Select(src4);
		dropdown4.selectByVisibleText("1990");		
		WebElement src5 = driver.findElementById("userRegistrationForm:occupation");
		Select dropdown5 = new Select(src5);
		dropdown5.selectByVisibleText("Public");		

		driver.findElementById("userRegistrationForm:mobile").sendKeys("1233455678");
		driver.findElementById("userRegistrationForm:address").sendKeys("1");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600092",Keys.TAB);
		Thread.sleep(2000);
		driver.findElementById("userRegistrationForm:landline").sendKeys("23765146");
		WebElement src8 = driver.findElementById("userRegistrationForm:cityName");
		Select dropdown8 = new Select(src8);
		dropdown8.selectByIndex(1);
		WebElement src9 = driver.findElementById("userRegistrationForm:postofficeName");
		Select dropdown9 = new Select(src9);
		dropdown9.selectByIndex(1);


	}

}
