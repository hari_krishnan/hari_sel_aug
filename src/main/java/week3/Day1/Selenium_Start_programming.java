package week3.Day1;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Selenium_Start_programming {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("TEST 11");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("TEST 11");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("TEST 11");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("TEST 11");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("TEST 11");
		driver.findElementById("createLeadForm_departmentName").sendKeys("TEST 11");
		driver.findElementById("createLeadForm_firstName").sendKeys("Test 21");
		driver.findElementById("createLeadForm_lastName").sendKeys("Test 31");
		//create web elements to access the values in a list
		// select by visible text
		WebElement src = driver.findElementById("createLeadForm_dataSourceId");
		Select dropdown = new Select(src);
		dropdown.selectByVisibleText("Direct Mail");
		
		//select by Index
		WebElement src1 = driver.findElementById("createLeadForm_currencyUomId");
		Select dropdown1 = new Select(src1);
		List<WebElement> options1 = dropdown1.getOptions();
		int size = options1.size();
		dropdown1.selectByIndex(size-2);
		
		//print the list of values in a drop down
		WebElement src2 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dropdown2 = new Select(src2);
		List<WebElement> options2 = dropdown2.getOptions();
		for (WebElement eachoption : options2) 
		{
			System.out.println(eachoption.getText());
			
		}
		driver.findElementByClassName("smallSubmit").click();
		//driver.findElementByLinkText("Logout").click();
	}

}
