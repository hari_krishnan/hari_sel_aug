package week3.Day2;

import java.util.Scanner;

public class DuplicatedNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scr = new Scanner(System.in);
		int array[] ;
		System.out.println("Enter the length of the array");
		int length = scr.nextInt();
		array = new int[length];
		System.out.println("Enter the array");
		for(int i=0;i<length;i++)
		{
			array[i]= scr.nextInt();
		}
		for(int i=0;i<length;i++)
		{
			for(int j=0;j<i;j++)
			{
				if(array[i]==array[j])
				{
					System.out.println(array[j]+" is duplicate");
					break;
				}
			}
		}
	}

}
