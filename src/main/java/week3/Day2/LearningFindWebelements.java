package week3.Day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearningFindWebelements {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leafground.com/pages/table.html");
		driver.manage().window().maximize();
		List<WebElement> Checkboxes = driver.findElementsByXPath("//input[@type = 'checkbox']");
		//Click the last check box in the web table
		int size =Checkboxes.size();
		System.out.println("# of check boxes"+size);
		Checkboxes.get(size-1).click();
	
		//Check checkboxes with 80% progress
		WebElement table= driver.findElementByXPath("//section[@class = 'innerblock']//table");
	
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		
		System.out.println(rows.size());
		
		for (int i = 1; i < rows.size()-1; i++) {
			
			String text = rows.get(i).findElements(By.tagName("td")).get(1).getText();
			
			if(text.contains("80")) {
				rows.get(i).findElements(By.tagName("td")).get(2).click();
			}
			
			
			
		}
		
		
		
		
		
		
		
		
		
		
		
		/*int table_size = table_data.size();
		System.out.println("table_size"+table_size);

		for(int i=0;i<table_size;i++)
		{
			WebElement each_row = table_data.get(i);
			List<WebElement> data = each_row.findElements(By.tagName("td"));
			String data_value = data.get(i).getText();
			System.out.println("data_value"+data_value);
			if(data_value=="80%")
			{
				Checkboxes.get(i).click();
			}
		}*/
	}

}
