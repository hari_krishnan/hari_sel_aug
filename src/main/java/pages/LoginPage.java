package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods{
	public LoginPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id = "username")
	WebElement eleUserName;
	public LoginPage typeUserName(String data)
	{
		//WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, data);
		return this;
	}
	
	@FindBy(id = "password")
	WebElement elePassword;
	public LoginPage typePassword(String data)
	{
		//WebElement eleUserName = locateElement("id", "username");
		type(elePassword, data);
		return this;
	}
	
	//@FindBy(className = "decorativeSubmit")
	//WebElement eleClick;
	
	@FindBy(how = How.CLASS_NAME,using = "decorativeSubmit")
	WebElement eleClick;
	public HomePage eleclickLogin()
	{
		click(eleClick);
		return new HomePage();
	}
}
