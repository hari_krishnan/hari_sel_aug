package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{
	public HomePage()
	{
		PageFactory.initElements(driver, this);
	}
	

	
	@FindBy(how = How.XPATH , using = "//div[@id='form']/h2")
	WebElement UserData;
	public MyHomePage verifyUserName(String data)
	{
		verifyPartialText(UserData, data);
		return new MyHomePage();
	}
}
