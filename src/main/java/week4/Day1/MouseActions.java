package week4.Day1;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class MouseActions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		Actions builder = new Actions(driver);
		driver.get("http://jqueryui.com/draggable/");
		WebElement findElementByXPath = driver.findElementByXPath("//iframe [@class = 'demo-frame']");
		driver.switchTo().frame(findElementByXPath);
		WebElement drag = driver.findElementById("draggable");
		//WebElement drop = driver.findElementById("droppable");
		//Point location = drop.getLocation();
		//thread.wait(3000);
		//builder.dragAndDrop(drag, drop).perform();
		//builder.pause(3000);
		builder.dragAndDropBy(drag, 100, 100).perform();
	}

}
