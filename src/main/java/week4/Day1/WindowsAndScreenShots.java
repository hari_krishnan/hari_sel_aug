package week4.Day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowsAndScreenShots {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./Drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.findElementByLinkText("Contact Us").click();
		//getting the window handle ID's of all open windows
		Set<String> allWindows = driver.getWindowHandles();
		List <String> listOfWindows = new ArrayList<>();
		listOfWindows.addAll(allWindows);
		//Switching the window
		driver.switchTo().window(listOfWindows.get(1));
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		//screenshot
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dsc = new File("./snaps/img.png");
		FileUtils.copyFile(src, dsc);
		//close 1st window keeping 2nd window open
		driver.switchTo().window(listOfWindows.get(0));
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		driver.close();	
		/*driver.get("https://www.irctc.co.in/nget/train-search");
		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.findElementByLinkText("Contact Us").click();*/
	}

}
