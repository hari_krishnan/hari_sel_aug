package week5.Day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReporting {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		//base level
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(false);
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		//test case level
		ExtentTest test = extent.createTest("TC001_CreateLead", "Create new Lead");
		test.assignAuthor("Hari");
		test.assignCategory("Smoke");
		//test step level
		test.pass("Browser lauched successfully");
		MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img.png");
		test.pass("Browser launched Successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img.png").build());
		test.pass("Entered user name successfully");
		test.pass("P/W entered succesfully");
		test.fail("Login unsuccessfull");
		//very very important step to run the entire code
		extent.flush();

	}

}

