package week.Day2;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class LearningMapImplementation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Map<Character,Integer> map = new HashMap<Character,Integer>();
		System.out.println("Enter your Name");
		Scanner scan = new Scanner(System.in);
		//int count= scan.nextInt();
		String Name = scan.nextLine();
		char [] name = Name.toCharArray();
		Integer val = null;
		for (char c : name) 
		{
			if(map.containsKey(c))
			{
				val = map.get(c)+1;
				map.put(c, val);
				System.out.println("Duplicate char "+ c);
			}
			else
			{
				map.put(c,1);
				//System.out.println("Values"+ map);
			}
			/*if(val>1)
			{
				System.out.println("Duplicate char "+ c);
			}*/
			
		}
	}

}
