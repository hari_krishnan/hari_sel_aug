package week.Day2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class LearningSetImplementation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Set <String> mobiles = new LinkedHashSet<String>();
		System.out.println("Enter the # of mobiles");
		Scanner scan = new Scanner(System.in);
		int count= scan.nextInt();
		scan.nextLine();
		for (int i =0;i<count;i++)
		{
			String mob = scan.nextLine();
			mobiles.add(mob);
		}
		System.out.println("No of mobiles "+mobiles.size());
		List <String> lsd = new ArrayList<String>();
		lsd.addAll(mobiles);
		System.out.println("The 1st mobile purchased is "+lsd.get(0));
	}

}
