package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class MergeLead extends ProjectMethods{
	@Test(groups= {"regression"})
	public void merge() throws InterruptedException
	{
		//login();
		WebElement eleLead = locateElement("linktext", "Leads");
		click(eleLead);
		//Navigate to merge lead
		WebElement mergeLead = locateElement("linktext", "Merge Leads");
		click(mergeLead);
		//select the icon to search for from lead
		WebElement from_lead = locateElement("xpath", "//img[@alt='Lookup']");
		click(from_lead);
		//switch to from lead window
		switchToWindow(1);
		//enter value and search for from lead	
		WebElement from_lead_value = locateElement("xpath","//input[@name='firstName']");
		type(from_lead_value,"gopi");
		WebElement find_leads_button = locateElement("xpath", "//button[text()='Find Leads']");
		click(find_leads_button);
		Thread.sleep(2000);
		String text = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").getText();
		System.out.println(text);
		WebElement firstLead = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		clickWithNoSnap(firstLead);
		switchToWindow(0);
		WebElement to_lead = locateElement("xpath", "(//img[@alt='Lookup'])[2]");
		click(to_lead);	
		switchToWindow(1);
		WebElement to_lead_value = locateElement("xpath","//input[@name='firstName']");
		type(to_lead_value,"babu");
		WebElement find_leadsbutton = locateElement("xpath", "//button[text()='Find Leads']");
		click(find_leadsbutton);
		Thread.sleep(2000);
		//select to lead value
		WebElement to_lead_value_select = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		clickWithNoSnap(to_lead_value_select);
		switchToWindow(0);
		//click on merge button
		WebElement Merge_button = locateElement("class", "buttonDangerous");
		click(Merge_button);
		acceptAlert();
		WebElement elefindlead = locateElement("linktext", "Find Leads");
		click(elefindlead);
		WebElement leadID = locateElement("xpath","//input[@name='id']");
		type(leadID,text);
		WebElement findleadsbutton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findleadsbutton);
		Thread.sleep(2000);
		WebElement verify = locateElement("class", "x-paging-info");
		verifyExactText(verify, "No records to display");
		closeBrowser();


	}


}
