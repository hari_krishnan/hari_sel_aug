package testcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class DeleteLead extends ProjectMethods
{
	@Test(groups= {"sanity"}, dependsOnGroups= {"smoke"})
	public void deleteLeads() throws InterruptedException
	{
		WebElement Leads = locateElement("linkText", "Leads");
		click(Leads);
		WebElement findLead = locateElement("linkText", "Find Leads");
		click(findLead);
		WebElement findLead_id = locateElement("xpath", "//input[@name='firstName']");
		type(findLead_id, "babu");
		WebElement findLead_button = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLead_button);
		Thread.sleep(2000);
		String text = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").getText();
		WebElement findLeadValue = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(findLeadValue);
		WebElement delete_link = locateElement("class", "subMenuButtonDangerous");
		click(delete_link);
		WebElement findLeads = locateElement("linkText", "Find Leads");
		click(findLeads);
		WebElement findLeadid = locateElement("xpath", "//input[@name='id']");
		type(findLeadid, text);
		WebElement findLeadbutton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadbutton);
		Thread.sleep(2000);
		WebElement verify = locateElement("class", "x-paging-info");
		verifyExactText(verify, "No records to display");
	}


}
