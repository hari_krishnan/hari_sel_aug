package week6.Day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadingExcel {

	@SuppressWarnings("resource")
	public static Object[][] readExcel(String fileName) throws IOException {
		// TODO Auto-generated method stub

		
		XSSFWorkbook wbook =new XSSFWorkbook("./Data/"+fileName+".xlsx");
		XSSFSheet sheet = wbook.getSheetAt(0);
		int row_count = sheet.getLastRowNum();
		short col_count = sheet.getRow(0).getLastCellNum();
		System.out.println("Row Count = "+row_count +"and Column Count =" + col_count);
		Object[][] data = new Object[row_count][col_count];
		for(int i=1;i<=row_count;i++)
		{
			XSSFRow row = sheet.getRow(i);
			for(int j=0;j<col_count;j++)
			{
				XSSFCell cell = row.getCell(j);
				data[i-1][j] = cell.getStringCellValue();
				//System.out.println(data);
			}
		}
		
		return data;
		
	}

}
